import * as fs from 'fs';
import htmlParse, {HTMLElement} from 'node-html-parser';

interface Lanzamiento {
    cuenta: string,
    garantia: string,
    opcion: string,
    neto: number,
    cubierto: "" | "CUBIERTO" | "D x 2" | "D x 1",
}

export interface LanzamientoAgrupado {
    cuenta: string;
    opcion: string;
    netos: {
        dx1: number,
        dx2: number,
        descubierto: number
    }
}

function agruparLanzamientos(lanzamientos: Lanzamiento[]): LanzamientoAgrupado[] {
    const lanzamientosAgrupados: Map<string, LanzamientoAgrupado> = new Map();

    function getLanzamientoAgroupadoOf(cuenta:string, opcion:string) {
        const key = cuenta + "$" + opcion;
        let la = lanzamientosAgrupados.get(key);
        if ( la == null ) {
            la = {
                cuenta: cuenta,
                opcion: opcion,
                netos: {
                    dx1: 0,
                    dx2: 0,
                    descubierto: 0
                }
            }
            lanzamientosAgrupados.set(key, la);
        }
        return la;
    }

    for ( let lanzamiento of lanzamientos ) {
        const cubierto = lanzamiento.cubierto;
        if (cubierto == "CUBIERTO" || lanzamiento.opcion[3] != 'C') {
            continue;
        }

        let la = getLanzamientoAgroupadoOf(lanzamiento.cuenta, lanzamiento.opcion);
        
        if (cubierto == "D x 1") {
            la.netos.dx1 += lanzamiento.neto;
        }else if (cubierto == "D x 2") {
            la.netos.dx2 += lanzamiento.neto;
        }else if (cubierto == "") {
            la.netos.descubierto += lanzamiento.neto;
        }
    }
    return Array.from(lanzamientosAgrupados.values());
}

export function parseEmail(contents: string) {
    const index = contents.indexOf('<TABLE border="0" cellspacing="2" cellpadding="4">');
    if ( index < 0 ) {
        throw new Error("Invalid mail format");
    }

    let htmlContents = contents.substring(index);

    const html = htmlParse(contents) as HTMLElement;

    const table = html.querySelector("TABLE");
    const trs = table.querySelectorAll("tr").slice(2);

    const data = trs.map( function(tr) {
        const tds = tr.querySelectorAll("td").map(td=>td.text.trim());

        return {
            cuenta: tds[0],
            garantia: tds[1],
            opcion: tds[2],
            neto: parseInt(tds[3]),
            cubierto: tds[4]
        } as Lanzamiento
    });

    return agruparLanzamientos(data);
}

//let fileContents = fs.readFileSync('./lanzamientos.eml', {encoding: 'utf8'});
//console.log(agruparLanzamientos(parseEmail(fileContents)));
