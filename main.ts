import * as puppeteer from 'puppeteer';
import { LanzamientoAgrupado, parseEmail } from './email-parser';
import * as fs from 'fs';
import {credentials} from './credentials';

////// FUNCIÓN CLICKANDWAITFORNAVIGATION -> Cuando ejecuta un "click", espera a que la página este navegable antes de continuar el proceso

async function clickAndWaitForNavigation(page: puppeteer.Page, selector:string) {
    let wait = page.waitForNavigation();
    await page.click(selector);
    await wait;
}

////// FUNCIÓN LOGIN -> Logea el usuario indicado y deja la página lista para navegar

async function login(page: puppeteer.Page, username: string, password: string) {
    await page.goto("https://posicionesygarantias.sba.com.ar/posicionesygarantias/service/login")
    await page.type('#j_username',username)
    await page.type('#j_password',password)
    await clickAndWaitForNavigation(page,'#startButton');
}

////// FUNCIÓN AJUSTE DE POSICIÓN -> Ingresa al menu "Total Distribuido" e ingresa a cada comitente parametrizado en la lista "Lanzamiento Agrupado" y ajusta la posición lanzadora.

async function adjustPosition(page: puppeteer.Page, lanzamiento: LanzamientoAgrupado) {
    await page.goto("https://posicionesygarantias.sba.com.ar/posicionesygarantias/service/totalDistribuidoFiltro");
    await page.select('#formaOperativa','OPCIONES_SOBRE_TITULOS_VALORES');
    await page.type('#codigoComitente',lanzamiento.cuenta);
    await clickAndWaitForNavigation(page,'input[type=submit][value="Buscar"]');

    let waitForNavigation = page.waitForNavigation();

    ////// Ingresa a la posición a ajustar del comitente

    page.evaluate(function(opcion) {
        let rows = Array.from(document.querySelectorAll('tr[id^=TotalDistribuidoOpcionesTVTableController]')) as HTMLElement[];
        let row = rows.find( function(row) {
            return row.children[3].textContent == opcion;
        } );

        row.click();
    }, lanzamiento.opcion);

    await waitForNavigation;

    async function gotoDistribucionTable() {
        await page.goto("https://posicionesygarantias.sba.com.ar/posicionesygarantias/service/distribucionTable");
    }

    await gotoDistribucionTable();
    
    ////// Da de baja TODAS las distribuciones de tipo "Lanzador" y status "Confirmada" para la especie seleccinada.
        ////// FUNCIÓN "findAndClickLanzamientoConfirmado" -> Identifica las distribuciones de la tabla que debe seleccionar.

    async function findAndClickLanzamientoConfirmado() {
        let waitForNavigation = page.waitForNavigation();
        let found = await page.evaluate(function() {
            let rows = Array.from(document.querySelectorAll('tr[id^=DistribucionTableController]')) as HTMLElement[];

            let row = rows.find( function(row) {
                return row.children[6].textContent == 'Lanzador' && row.children[10].textContent == "Confirmada";
            } );

            if (row) {
                row.click();
                return true;
            }
            return false;
        });

        if (found) {
            await waitForNavigation;
        }
        
        return found;
    }
    
        ////// Da de baja la distribución seleccionada por la formula "FindAndClickLanzamientoConfirmado" cuando encuentra fila

    console.log(`Bajando distribuciones de ${lanzamiento.opcion} para cc ${lanzamiento.cuenta}`);
    while ( await findAndClickLanzamientoConfirmado() ) {
        await clickAndWaitForNavigation(page, 'input[type=submit][value="Baja Distribución"]');
        await page.goto("https://posicionesygarantias.sba.com.ar/posicionesygarantias/service/distribucionTable")
    }
    
    ////// Cuando todas las distribuciones son dadas de baja, redistribuye las posiciones segun sean "Cubierto", "Descubierto", "Dx1" o "Dx2"

    await clickAndWaitForNavigation(page, 'input[type=button][value="Ir a Total Distribuido"]');

    let totalADistribuir = await page.evaluate(function(){
        return parseInt(document.querySelector("#formTable").querySelectorAll("tr")[0].children[1].textContent.trim())
    });
    console.log(`Redistribuyendo opciones ${lanzamiento.opcion}`);

    await clickAndWaitForNavigation(page, 'input[type=button][value="Distribuir..."]');

    let indexPedido = 0;

    async function agregarPedido(cantidadOpciones:number,tipoLanzador:string){
        await clickAndWaitForNavigation(page, 'input[type=submit][value="Agregar Pedido"]');
        await page.type(`[id="pedidosDistribucionLanzador[${indexPedido}].cantidadOpciones"]`,cantidadOpciones.toString());
        await page.select(`[id="pedidosDistribucionLanzador[${indexPedido}].tipoLanzador"]`,tipoLanzador);
        indexPedido += 1;
    }

    if ( lanzamiento.netos.descubierto != 0 ) {
        await agregarPedido(lanzamiento.netos.descubierto,"DESCUBIERTO");
        console.log(`Lotes modificados a Descubierto: ${lanzamiento.netos.descubierto}`)
    }

    if ( lanzamiento.netos.dx1 != 0 ) {
        await agregarPedido(lanzamiento.netos.dx1,"DESCUBIERTO_24");
        console.log(`Lotes modificados a Dx1: ${lanzamiento.netos.dx1}`);
    }

    if(  lanzamiento.netos.dx2 != 0 ) {
        await agregarPedido(lanzamiento.netos.dx2,"DESCUBIERTO_48");
        console.log(`Lotes modificados a Dx2: ${lanzamiento.netos.dx2}`);  
    }
    let cubiertos = totalADistribuir - (lanzamiento.netos.descubierto + lanzamiento.netos.dx1 + lanzamiento.netos.dx2);
    
    if(  cubiertos != 0 ) {
        await agregarPedido(cubiertos,"CUBIERTO"); 
        console.log(`Lotes reestablecidos a Cubierto: ${cubiertos}`);
    }
    
    await clickAndWaitForNavigation(page, 'input[type=submit][value="Distribuir"]');
    
}

////// FUNCIÓN MAIN -> Invoca las funciones en orden para correr el proceso. Tambien define el usuario y contraseña para logear en la página

async function main() {

    let fileContents = fs.readFileSync('./lanzamientos.eml', {encoding: 'utf8'});
    const lanzamientos = parseEmail(fileContents);
    const browser = await puppeteer.launch({executablePath: "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe", headless: false});
    const page = await browser.newPage();
    
    page.on('dialog', function(dialog) {
        dialog.accept();
    });

    await login(page,credentials.user,credentials.password);
    let cantidadExitosa = 0
    console.log("Iniciando ajustes de posición");

    for ( let lanzamiento of lanzamientos ) {
        try {
            await adjustPosition(page, lanzamiento);
            cantidadExitosa += 1
        }catch(e) {
            console.log("No se pudo distribuir: cc "+ lanzamiento.cuenta + " para la opción "+lanzamiento.opcion);
        }
        await new Promise(resolve =>setTimeout(resolve, 0));
    }
    await browser.close();
    console.log(`Se realizaron con éxito ${cantidadExitosa} ajustes de posición y se encontraron ${lanzamientos.length - cantidadExitosa} errores`)
}

main();